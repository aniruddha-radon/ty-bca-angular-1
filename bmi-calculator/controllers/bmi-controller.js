app.controller("bmiController", function ($scope) {
    $scope.weight = 50;
    $scope.height = 5.0;

    $scope.BMI = '';
    $scope.message = '';

    $scope.increaseWeight = () => {
        $scope.weight++;
        $scope.calculateBMI();
    }

    $scope.decreaseWeight = () => {
        $scope.weight--;
        $scope.calculateBMI();
    }

    $scope.increaseHeight = () => {
        $scope.height += 0.1;
        $scope.calculateBMI();
    }
    $scope.decreaseHeight = () => {
        $scope.height -= 0.1;
        $scope.calculateBMI();
    }

    $scope.calculateBMI = () => {
        $scope.BMI = ($scope.weight / (($scope.height * 0.3048) ** 2));

        if ($scope.BMI > 30) {
            $scope.message = 'Obese';
        } else if ($scope.BMI > 25) {
            $scope.message = 'Overweight';
        } else if ($scope.BMI > 18) {
            $scope.message = 'Normal';
        } else {
            $scope.message = 'Underweight';
        }
    }

    $scope.calculateBMI();
})