app.controller("filterController", function($scope){
    $scope.pokemons = ["Charizard", "Blastoise", "Venausaur", "Mewtwo"];
    $scope.date = new Date();


    $scope.musicians = [
        {
            name: "Aniruddha",
            age: 27
        },
        {
            name: "Bach",
            age: 82
        },
        {
            name: "Beethoven",
            age: 71
        }
    ]
});