app.controller("imageEditorController", function ($scope) {

    $scope.imageStyle = {};

    $scope.handleChange = () => {

        $scope.imageStyle = {
            'filter': `brightness(${$scope.brightness}%) 
                        contrast(${$scope.contrast}%) 
                        grayscale(${$scope.grayscale}%) 
                        sepia(${$scope.sepia}%)`
        }

        
    }
})