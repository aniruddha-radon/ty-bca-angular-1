app.controller("revisionController", function($scope, dataService) {
    $scope.users = [];

    $scope.getUsers = () => {
        dataService.getUsers().then(function(response) {
            $scope.users = response.data;
        })
    }

    $scope.getUsers();
});