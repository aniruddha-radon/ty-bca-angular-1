app.service("dataService", function($http){
    this.getUsers = () => {
        return $http.get("https://jsonplaceholder.typicode.com/users");
    }
});


// DRY
// - DO NOT REPEAT YOURSELF

// KISS
// - KEEP IT SIMPLE SILLY
