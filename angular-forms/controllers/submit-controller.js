app.controller("submitController", function ($scope) {
    //the $scope is the model. it acts as the glue between the view (HTML) and the controller.
    //Anything specified on the model is accessible in the view.
    //Anything NOT specified on the model is NOT accessible in the view.

    $scope.name = ""; // variable name is available in the view.
    var name = "Jane Doe"; // NOT available in the view.

    $scope.number = "";
    $scope.password = "";

    $scope.isSubmitted = false;

    // defining a function called submit.
    // for the function to be available in the view we will define it on the $scope
    $scope.submit = function() {
        $scope.isSubmitted = true;
        if($scope.name.trim() && $scope.password.trim() && $scope.number.trim()) {
            alert("form is valid");
        }
    }
})